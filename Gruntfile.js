'use strict';

module.exports = function (grunt) {
    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({
        // Project settings
        config: {
            styles:     'assets/styles',
            scripts:    'assets/scripts',
            images:     'assets/images'
        },
        watch: {
            css: {
                files: ['<%= config.styles %>/**/styles.scss'],
                tasks: ['sass', 'autoprefixer'],
                options: {
                    livereload: true
                }
            },
            js: {
                files: ['Gruntfile.js','<%= config.scripts %>/**/*.js'],
                tasks: ['jshint', 'sass', 'autoprefixer']
            }
        },
        sass: {
            defaults: {
                options: {
                    style: 'compact'
                },
                files: {
                    '<%= config.styles %>/styles.css':'<%= config.styles %>/styles.scss'
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 2 version']
            },
            defaults: {
                expand: true,
                flatten: true,
                src: '<%= config.styles %>/styles.css',
                dest: '<%= config.styles %>/'
            }

        },
        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                '<%= config.scripts %>/**/*.js'
            ],
            globals: {
                jQuery: true,
                console: true,
                module: true
            }
        }
    });

    grunt.registerTask('build', [
        'sass',
        'autoprefixer',
        'jshint',
        'watch'
    ]);
};
