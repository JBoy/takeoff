<?php

$isLocalhost = ($_SERVER['REMOTE_ADDR'] == '127.0.0.1');
$dbenv = ($isLocalhost) ? 'local':'remote';
$useDB = FALSE;
$logErrors = TRUE;

if($isLocalhost){
	error_reporting(E_ALL);
}

if($logErrors){
	Flight::set('flight.log_errors', true);
}

if($useDB){
	$dbSetup = array(
		'local' => array(
			'host' => 'localhost',
			'dbname' => 'database',
			'dbuser' => 'root',
			'dbpass' => 'root'
		),
		'remote' => array(
			'host' => '',
			'dbname' => '',
			'dbuser' => '',
			'dbpass' => ''
		)
	);

	RedBean_Facade::setup('mysql:host='. $dbSetup[$dbenv]['host'] .';dbname='. $dbSetup[$dbenv]['dbname'],$dbSetup[$dbenv]['dbuser'],$dbSetup[$dbenv]['dbpass']);
}