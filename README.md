# TAKEOFF #

### What is this repository for? ###

* Quick Boostrapping a small PHP app 

### Components ###

* [FlightPHP](http://flightphp.com/learn)
* [RedbeanPHP](http://flightphp.com/learn)
* [Bootstrap](http://getbootstrap.com/getting-started)
* [SASS](http://sass-lang.com/guide)
* [Composer](https://getcomposer.org/doc/00-intro.md)
* [Grunt](http://gruntjs.com/getting-started)
* [Bower](http://bower.io/)

### How do I get set up? ###

* Run ./install.sh


### Database configuration ###

* Set the `$useDB` to `TRUE` in config/config.php and your credentials in the `$dbSetup` array
* Done and Done. Get your groove on with RedbeanPHP

### Todo ###