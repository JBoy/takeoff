<?php
require 'vendor/autoload.php';
include 'config/config.php';
use RedBean_Facade as R;
Flight::set('flight.views.path', 'views');
Flight::route('/', function(){

	Flight::render('header', array('heading' => 'heading'), 'header_content');
	Flight::render('body', array('body' => 'body'), 'body_content');
	Flight::render('footer', array('footer' => 'footer'), 'footer_content');
	Flight::render('layout', array('title' => 'Home Page'));

});




Flight::route('*', function(){
	Flight::redirect('/');
	// Do something
});













Flight::start();
