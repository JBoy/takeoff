<!DOCTYPE html>
<html lang="sv">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Leos farsa - <?php echo $title; ?></title>
	<meta name="description" content="">
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<link rel="stylesheet" href="assets/styles/style.css?v=1">
</head>
<body>
	<div id="container">
		<?php echo $header_content; ?>
		<?php echo $body_content; ?>
		<?php echo $footer_content; ?>
	</div>

	<script src="/assets/scripts/jquery-2.0.3.min.js"></script>
	<script src="/assets/scripts/script.js?v=1"></script>
</body>
</html>