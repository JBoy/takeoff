<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="sv" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="sv" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="sv" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="sv" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="sv" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php echo $title; ?></title>
	<meta name="description" content="">
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/assets/styles/styles.css?v=1">
	<script src="/bower_components/modernizr/modernizr.js"></script>
</head>
<body>
	<div class="container">
		<?php echo $header_content; ?>
		<?php echo $body_content; ?>
	<?php echo $footer_content; ?>
	</div>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script>!window.jQuery && document.write('<script src="/bower_components/jquery/jquery.min.js"><\/script>')</script>
	<script src="/assets/scripts/script.js?v=1"></script>
</body>
</html>
