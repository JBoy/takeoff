

	<div class="masthead">
		<h3 class="text-muted"><?php echo $heading; ?></h3>
		<nav>
			<ul class="nav nav-justified">
				<li class="active"><a href="#">Home</a></li>
				<li><a href="#">Projects</a></li>
				<li><a href="#">Services</a></li>
				<li><a href="#">Downloads</a></li>
				<li><a href="#">About</a></li>
				<li><a href="#">Contact</a></li>
			</ul>
		</nav>
	</div>

	
	<!-- Jumbotron -->
	<div class="jumbotron">
		<h1>Marketing stuff!</h1>
		<p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet.</p>
		<p><a class="btn btn-lg btn-success" href="#" role="button">Get started today</a></p>
	</div>
